(function ($) {

 /**
   * UI Improvements for the Content Editing Form
   */
 Drupal.behaviors.paf = {
   attach: function (context, settings) {
     var width = $('#node-edit #edit-title').width() - $('#node-edit .form-item-path-alias label').width() - $('#node-edit .form-item-path-alias .field-prefix').width() - 10;
     $('#node-edit .form-item-path-alias input').css('width', width);

     if ($('#node-edit .form-item-body-und-0-value label').html() == 'Body <span class="form-required" title="This field is required.">*</span>') {
       $('#node-edit .form-item-body-und-0-value label').html('');
       $('#node-edit .form-item-body-und-0-value label').css('text-align', 'right');
     }
   }
 }

})(jQuery);
