<?php

/**
 * @file
 * Administration fixes.
 */

/**
 * Implements hook_form_alter()
 */
function paf_form_alter(&$form, &$form_state, $form_id) {
  // Improve the title field.
  if (isset($form['title']['#title'])) {
    $form['title']['#attributes']['placeholder'] = $form['title']['#title'];
  }

  // Improve the URL selection form.
  if (isset($form['#entity_type']) && $form['#entity_type'] == 'node' && isset($form['path']) && isset($form['path']['alias'])) {
    // Remove the fieldset.
    unset($form['path']['#type']);
    unset($form['path']['alias']['#description']);
    $form['path']['alias']['#title'] = '<strong>' . t('Permalink') . ':</strong> ';
    $form['path']['alias']['#field_prefix'] = (isset($_SERVER['HTTPS'])) ? 'https://' . $_SERVER['HTTP_HOST'] . '/' : 'http://' . $_SERVER['HTTP_HOST'] . '/';

    if (isset($form['path']['pathauto'])) {
      $form['path']['pathauto']['#access'] = FALSE;
      // Alter the place holder text based on the pathauto default value.
      $form['path']['alias']['#attributes'] = array('placeholder' => '<' . t('magically generated') . '>');
      $form['path']['alias']['#states'] = array();
      $form['#submit'][] = 'paf_pathauto_submit';
    }

    // Move to below the title.
    unset($form['path']['#group']);
    $form['path']['#type'] = 'container';
  }

  // Add clearfix to node body container to prevent clearing issue with
  // vertical tabs - @see https://drupal.org/node/1686500
  if (isset($form['#entity_type']) && $form['#entity_type'] == 'node' && isset($form['body']['#type']) && $form['body']['#type'] == 'container') {
    $form['body']['#attributes']['class'][] = 'clearfix';
  }

  // Improve the publishing option labels.
  if (isset($form['options'])) {
    $form['options']['promote']['#title'] = t('Promote');
    $form['options']['sticky']['#title'] = t('Featured');
  }

  // Improve authoring options.
  if (isset($form['author']) && isset($form['#entity_type']) && $form['#entity_type'] == 'node') {
    unset($form['author']['name']['#title']);
    $form['author']['name']['#description'] = t('Enter the author username. Leave blank for anonymous.');
    $form['author']['name']['#attributes']['placeholder'] = t('Username');
    $form['author']['name']['#size'] = '40';
    $form['author']['name']['#weight'] = 4;
    $form['author']['date']['#weight'] = 5;

    // Adjust options for Date Popup Module.
    $form['author']['date']['#title_display'] = 'invisible';
    $form['author']['date']['#size'] = 20;

    unset($form['author']['#group']);
    $form['author']['#title'] = t('Author');
    $form['author']['#collapsible'] = FALSE;
    $form['author']['#collapsed'] = FALSE;
  }

  // Improve the revision interface.
  if (!empty($form['revision_information']['revision']['#access'])) {
    $form['revision_information']['log']['#states'] = array(
      'visible' => array(
        'input[name="revision"]' => array('checked' => TRUE)
      )
    );
    $form['revision_information']['log']['#weight'] = 10;
    $form['revision_information']['log']['#title_display'] = 'invisible';
    $form['revision_information']['revision']['#weight'] = 9;
    $form['options']['revision'] = $form['revision_information']['revision'];
    $form['options']['log'] = $form['revision_information']['log'];
    unset($form['revision_information']['#type']);
    unset($form['revision_information']['log']);
    unset($form['revision_information']['revision']);
  }

  if (!empty($form['options']['#access'])) {
    unset($form['options']['#group']);
    $form['options']['#title'] = t('Options');
    $form['options']['#collapsible'] = FALSE;
    $form['options']['#collapsed'] = FALSE;
  }

  // Improve the menu adding experience.
  if (isset($form['menu']) && isset($form['menu']['enabled']) && !empty($form['menu']['#access'])) {
    $form['menu']['link']['link_title']['#size'] = '';
    $form['menu']['link']['weight']['#access'] = FALSE;
    $form['menu']['link']['link_title']['#title'] = t('Link title');
    $form['menu']['link']['parent']['#title'] = t('Parent');
    unset($form['menu']['#group']);
    $form['menu']['#title'] = t('Menu');
    $form['menu']['#collapsible'] = FALSE;
    $form['menu']['#collapsed'] = FALSE;
    $form['#validate'][] = 'paf_menu_validate';
  }

  // Add fields to the rubik sidebar.
  $sidebar_elements = array('author', 'options', 'menu');
  $weight = 999;
  foreach ($sidebar_elements as $element) {
    if (isset($form[$element])) {
      $form[$element]['#attributes']['class'][] = 'rubik_sidebar_field';
      $form[$element]['#weight'] = $weight++;
    }
  }
}

/**
 * Submit function for adding pathauto settings
 */
function paf_pathauto_submit($form, &$form_state) {
  $values = $form_state['values'];
  $autogenerate = TRUE;

  // If the submitted 'alias' is NOT empty then we potentially may not want to
  // auto-generate.
  if (!empty($values['path']['alias'])) {
    // If the submitted alias differs than the previous form default value then
    // the user is attempting to manually set it.
    if ($values['path']['alias'] != $form['path']['alias']['#default_value']) {
      $autogenerate = FALSE;
    }
    // If the submitted value is equal to the previous value AND the
    // autogenerate (pathauto) setting is FALSE then an alias was previously
    // manually set so leave it.
    elseif (!empty($form['path']['pathauto']) && !$form['path']['pathauto']['#default_value']) {
      $autogenerate = FALSE;
    }
  }

  $form_state['values']['path']['pathauto'] = $autogenerate;
  $form_state['input']['path']['pathauto'] = $autogenerate;
}

/**
 * Validate function for adding menu validation
 */
function paf_menu_validate($form, &$form_state) {
  if ($form_state['input']['menu']['enabled'] && !$form_state['input']['menu']['link_title']) {
    form_set_error('link_title', t('Menu link title is required.'));
  }
}

/**
 * Implements hook_menu_alter().
 */
function paf_menu_alter(&$items) {
  // Adjust the "Configuration" page to only show when the user can change the
  // configuration options
  $items['admin/config']['access arguments'] = array('administer site configuration');
}
